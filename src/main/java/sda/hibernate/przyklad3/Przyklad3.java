package sda.hibernate.przyklad3;

import org.hibernate.Session;
import org.hibernate.query.Query;
import sda.hibernate.model.Address;

import java.util.List;

import static sda.hibernate.util.HibernateUtil.getSessionFactory;

public class Przyklad3 {

    public static void main(String[] args) {
        Session session = getSessionFactory().openSession();
        Query<Address> addressQuery = session.createQuery(
                "select a from Address a where a.street= :street",
                Address.class)
                .setParameter("street", "Testowa");

        List<Address> addresses = addressQuery.getResultList();
        addresses.forEach(address ->
                System.out.println(address.getId() +" "+ address.getCity()));

        Query<Address> joinQuery = session.createQuery(
                "select a from Address a join fetch a.country c where c.alias= :alias",
                Address.class)
                .setParameter("alias", "IN");
        List<Address> joinAdresses = joinQuery.getResultList();
        session.close();
        //jeżeli będzie zwykły join to będzie LazyInitializationException - bo dane z Country nie zostaną zaciągnięte
        //jeżeli będzie join fetch - to wszystko będzie ok
        joinAdresses.forEach(address ->
                System.out.println(address.getCountry() +" "+ address.getCity()));

    }

}
