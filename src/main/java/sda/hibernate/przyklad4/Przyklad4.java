package sda.hibernate.przyklad4;

import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import sda.hibernate.model.Country;

import static sda.hibernate.util.HibernateUtil.getSessionFactory;

public class Przyklad4 {
    public static void main(String[] args) {
        Session session = getSessionFactory().openSession();
        NativeQuery<Country> nativeQuery = session.createNativeQuery(
                "select * from Country where co_name=:name",
                Country.class)
                .setParameter("name", "Japan");

        nativeQuery.getResultList()
                .forEach(country -> System.out.println(country.toString()));
        session.close();
    }
}
