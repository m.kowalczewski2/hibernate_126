package sda.hibernate.przyklad1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import sda.hibernate.model.Address;
import sda.hibernate.model.Country;

public class Przyklad1 {

    public static void main(String[] args) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Address address = session.find(Address.class, 1);
        //jeżeli jest lazy to wywoła nam się tutaj drugie query, wyciągające country
        //jeżeli zawołamy tutaj country podczas lazy, to nie dostaniemy LazyInitializationException
        //w linijce 23
        //System.out.println(address.getCountry());
        session.close();
        sessionFactory.close();
        //jeżeli jest lazy, i nie zawołaliśmy wcześniej country to dostaniemy tutaj LazyInitializationException
        //jeżeli jest eager to wszystko będzie ok - wykona się query z joinem
        //System.out.println(address.getCountry());
    }

}
