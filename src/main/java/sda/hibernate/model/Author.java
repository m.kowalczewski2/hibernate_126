package sda.hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@NoArgsConstructor
//w tym przypadku nie użyliśmy @Data - żeby uniknąć niepotrzebnego kodu od lomboka
//np toString, equals, hashCode - wszystkie one wołają pole - books
@Getter
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    //Jeżeli przy many to many chcemy zastosować eager - to starajmy się
    //aby to było tylko po jednego stronie relacji, zastosowanie eager
    //po obu stronach może prowadzić do pętli wazajemnych wywołań
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "books_authors",
            joinColumns = {@JoinColumn(name = "author_id")}, //klucz obcy w tabeli books_authors - wskazujący na encję w której obecnie się znajdujemy - czyli na Author
            inverseJoinColumns = {@JoinColumn(name = "book_id")} //klucz obcy w tabeli books_authors - wskazujący na tabelę Books
    )
    private Set<Book> books;

}
