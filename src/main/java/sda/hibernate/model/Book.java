package sda.hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@NoArgsConstructor
//w tym przypadku nie użyliśmy @Data - żeby uniknąć niepotrzebnego kodu od lomboka
//np toString, equals, hashCode - wszystkie one wołają pole - authors
@Getter
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @ManyToMany(mappedBy = "books", fetch = FetchType.LAZY)
    private Set<Author> authors;

}
