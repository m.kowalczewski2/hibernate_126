package sda.hibernate.zadanie2;

import sda.hibernate.dao.GenericDao;
import sda.hibernate.dao.GenericDaoImpl;
import sda.hibernate.model.Address;
import sda.hibernate.model.Country;

public class Zadanie2 {

    public static void main(String[] args) {
        GenericDao<Address> addressDao = new GenericDaoImpl<>(Address.class);
        GenericDao<Country> countryDao = new GenericDaoImpl<>(Country.class);
        addressDao.findById(1).ifPresent(address -> System.out.println(address.getCity()));
        countryDao.findById(1).ifPresent(country -> System.out.println(country.getName()));
        Address address = new Address();
        address.setPostalCode("85-355");
        address.setApartmentNo("85");
        address.setBuildingNo("8");
        address.setStreet("Testowa");
        address.setCity("Testowe");
        countryDao.findById(3).ifPresent(address::setCountry);
        addressDao.insert(address);

        countryDao.getAll().forEach(System.out::println);
    }

}
