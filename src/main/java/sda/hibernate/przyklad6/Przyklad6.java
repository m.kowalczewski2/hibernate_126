package sda.hibernate.przyklad6;

import org.hibernate.Session;
import sda.hibernate.model.Author;

import static sda.hibernate.util.HibernateUtil.getSessionFactory;

public class Przyklad6 {

    public static void main(String[] args) {
        //wiazania many to many - przykład jak to działa - przykłady sytuacji
        //kiedy może dojść do stackoverflow z powodu pętli wzajemnych zapytań
        //encje Book i Author
        Session session = getSessionFactory().openSession();

        Author author = session.find(Author.class, 1);
        author.getBooks().forEach(book -> System.out.println(book.getName()));

        session.close();
    }



}
