package sda.hibernate.dao;

import java.util.List;
import java.util.Optional;

public interface GenericDao<T> {

    Optional<T> findById(int id);
    void insert(T t);
    void delete(T t);
    List<T> getAll();


}
