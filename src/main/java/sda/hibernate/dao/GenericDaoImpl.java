package sda.hibernate.dao;

import org.hibernate.Session;
import sda.hibernate.model.Address;

import java.util.List;
import java.util.Optional;

import static sda.hibernate.util.HibernateUtil.getSessionFactory;

public class GenericDaoImpl<T> implements GenericDao<T> {

    private final Class<T> modelClass;

    public GenericDaoImpl(Class<T> modelClass) {
        this.modelClass = modelClass;
    }

    @Override
    public Optional<T> findById(int id) {
        Session session = getSessionFactory().openSession();
        T result = session.find(modelClass, id);
        session.close();
        return Optional.ofNullable(result);
    }

    @Override
    public void insert(T t) {
        Session session = getSessionFactory().openSession();
        session.beginTransaction();
        session.persist(t);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void delete(T t) {
        Session session = getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(t);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public List<T> getAll() {
        try(Session session = getSessionFactory().openSession()) {
            return session.createQuery(
                    "from "+modelClass.getName(),
                    modelClass
            ).getResultList();
        }
    }
}
