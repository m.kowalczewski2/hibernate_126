package sda.hibernate.dao;

import sda.hibernate.model.Order;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderDao {

    List<Order> findBeforeDate(LocalDateTime dateTime);

}
