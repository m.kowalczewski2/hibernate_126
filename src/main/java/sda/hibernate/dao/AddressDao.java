package sda.hibernate.dao;

import sda.hibernate.model.Address;

import java.util.Optional;

public interface AddressDao {

    Optional<Address> getAddressById(int id);

}
