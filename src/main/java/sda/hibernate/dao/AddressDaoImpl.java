package sda.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import sda.hibernate.model.Address;

import java.util.Optional;

public class AddressDaoImpl implements AddressDao {

    @Override
    public Optional<Address> getAddressById(int id) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Address address = session.find(Address.class, id);
        session.close();
        sessionFactory.close();
        return Optional.ofNullable(address);
    }

}
