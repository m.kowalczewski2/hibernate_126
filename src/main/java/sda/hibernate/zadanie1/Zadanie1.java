package sda.hibernate.zadanie1;

import sda.hibernate.dao.AddressDao;
import sda.hibernate.dao.AddressDaoImpl;
import sda.hibernate.model.Address;

public class Zadanie1 {
    public static void main(String[] args) {
        AddressDao addressDao = new AddressDaoImpl();
        addressDao.getAddressById(1).ifPresent(address -> System.out.println(address.getCity()));
    }
}
