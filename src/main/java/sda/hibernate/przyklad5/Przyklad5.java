package sda.hibernate.przyklad5;

import org.hibernate.Session;
import org.hibernate.query.Query;
import sda.hibernate.model.Address;


import static sda.hibernate.util.HibernateUtil.getSessionFactory;

public class Przyklad5 {

    public static void main(String[] args) {
        Session session = getSessionFactory().openSession();
        Query<Address> query = session.createNamedQuery("address.select.all", Address.class);
        query.getResultList().forEach(address -> System.out.println(address.getCity()));
        Query<Address> queryParameter = session.createNamedQuery("inne", Address.class)
                .setParameter("id", 2);
        queryParameter.getResultList().forEach(address -> System.out.println(address.getStreet()));
        session.close();
    }

}
