package sda.hibernate.dao;

import org.junit.jupiter.api.*;
import sda.hibernate.model.Country;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class GenericDaoImplIT {

    Country exampleCountry = new Country(1, "Poland", "PL");

    List<Country> exampleCountries = List.of(
            exampleCountry,
            new Country(2, "Germany", "DE"),
            new Country(3, "France", "FR"),
            new Country(4, "Russia", "RU")
    );

    @Test
    @Order(1)
    void shouldFindCountryWithId1FromDataScript() {
        GenericDao<Country> countryDao = new GenericDaoImpl<>(Country.class);
        Optional<Country> result = countryDao.findById(1);
        assertThat(result.get()).isEqualTo(exampleCountry);
    }

    @Test
    @Order(2)
    void shouldFindAllCountriesFromDataScript() {
        GenericDao<Country> countryDao = new GenericDaoImpl<>(Country.class);
        List<Country> result = countryDao.getAll();
        assertThat(result).containsAll(exampleCountries);
    }
/*
    @Test
    @Order(3)
    void shouldInsertNewCountryIntoCountryTable() {
        GenericDao<Country> countryDao = new GenericDaoImpl<>(Country.class);
        Country countryToBeAdded = new Country(null, "Ukraine", "UA");
        countryDao.insert(countryToBeAdded);
        assertThat(countryDao.getAll()).contains(countryToBeAdded);
    }
*/

}