INSERT INTO `Country` (`CO_NAME`, `CO_ALIAS`) VALUES ('Poland', 'PL');
INSERT INTO `Country` (`CO_NAME`, `CO_ALIAS`) VALUES ('Germany', 'DE');
INSERT INTO `Country` (`CO_NAME`, `CO_ALIAS`) VALUES ('France', 'FR');
INSERT INTO `Country` (`CO_NAME`, `CO_ALIAS`) VALUES ('Russia', 'RU');

INSERT INTO `Address` (`ADD_STREET`, `ADD_BUILDING_NO`, `ADD_APARTMENT_NO`, `ADD_CITY`, `ADD_POSTAL_CODE`, `ADD_CO_ID`) VALUES ('Gdanska', '32', '1', 'Bydgoszcz', '85-000', '1');
INSERT INTO `Address` (`ADD_STREET`, `ADD_BUILDING_NO`, `ADD_CITY`, `ADD_POSTAL_CODE`, `ADD_CO_ID`) VALUES (' rue de Raymond Poincaré', '96', 'Nantes', '44200', '3');
INSERT INTO `Address` (`ADD_STREET`, `ADD_BUILDING_NO`, `ADD_APARTMENT_NO`, `ADD_CITY`, `ADD_POSTAL_CODE`, `ADD_CO_ID`) VALUES ('Rue Joseph Vernet', '103', '5', 'BAIE-MAHAULT', '97122', '3');
INSERT INTO `Address` (`ADD_STREET`, `ADD_BUILDING_NO`, `ADD_APARTMENT_NO`, `ADD_CITY`, `ADD_POSTAL_CODE`, `ADD_CO_ID`) VALUES ('Schönhauser Allee', '73', '2', 'Wembach', '79677', '2');
INSERT INTO `Address` (`ADD_STREET`, `ADD_BUILDING_NO`, `ADD_APARTMENT_NO`, `ADD_CITY`, `ADD_POSTAL_CODE`, `ADD_CO_ID`) VALUES ('Metallistov Pr.', '10', '39', ' Sankt-Peterburg', '54583', '4');
INSERT INTO `Address` (`ADD_STREET`, `ADD_BUILDING_NO`, `ADD_CITY`, `ADD_POSTAL_CODE`, `ADD_CO_ID`) VALUES ('ul. Mokierska', '29', 'Mikołów', '43-190', '1');
INSERT INTO `Address` (`ADD_STREET`, `ADD_BUILDING_NO`, `ADD_APARTMENT_NO`, `ADD_CITY`, `ADD_POSTAL_CODE`, `ADD_CO_ID`) VALUES ('Al. Lotników Polskich', '75', '8', 'Świdnik', '21-045', '1');

INSERT INTO `Category` (`CAT_NAME`) VALUES ('Food');
INSERT INTO `Category` (`CAT_NAME`) VALUES ('Computers');
INSERT INTO `Category` (`CAT_NAME`) VALUES ('It Services');
INSERT INTO `Category` (`CAT_NAME`) VALUES ('Dishwashers');
INSERT INTO `Category` (`CAT_NAME`) VALUES ('Guitars');

INSERT INTO `User` (`USR_FIRST_NAME`, `USR_LAST_NAME`, `USR_EMAIL`, `USR_BIRTH_DATE`, `USR_ADD_ID`) VALUES ('Jan', 'Kowalski', 'jk@wp.pl', '1975-05-20', '1');
INSERT INTO `User` (`USR_FIRST_NAME`, `USR_LAST_NAME`, `USR_EMAIL`, `USR_BIRTH_DATE`, `USR_ADD_ID`) VALUES ('Stephen', 'Muller', 'sm@gmail.com', '1995-02-11', '4');
INSERT INTO `User` (`USR_FIRST_NAME`, `USR_LAST_NAME`, `USR_EMAIL`, `USR_BIRTH_DATE`, `USR_ADD_ID`) VALUES ('Maria', 'Curie-Sklodowska', 'mc@gmail.com', '1880-04-01', '2');

INSERT INTO `Product` (`PRO_NAME`, `PRO_DESCRIPTION`, `PRO_PRICE`, `PRO_CAT_ID`) VALUES ('Kanapka', 'Smaczna', '5.50', '1');
INSERT INTO `Product` (`PRO_NAME`, `PRO_DESCRIPTION`, `PRO_PRICE`, `PRO_CAT_ID`) VALUES ('Komputer', 'Mocny', '4000', '2');
INSERT INTO `Product` (`PRO_NAME`, `PRO_DESCRIPTION`, `PRO_PRICE`, `PRO_CAT_ID`) VALUES ('Pizza', 'Bez ananasa', '30', '1');
INSERT INTO `Product` (`PRO_NAME`, `PRO_DESCRIPTION`, `PRO_PRICE`, `PRO_CAT_ID`) VALUES ('Les Paul', 'Gibson', '3500', '5');
INSERT INTO `Product` (`PRO_NAME`, `PRO_DESCRIPTION`, `PRO_PRICE`, `PRO_CAT_ID`) VALUES ('Laptop', 'Slaby', '1500', '2');

INSERT INTO `Order` (`ORD_DATE`, `ORD_PRICE`, `ORD_USR_ID`) VALUES ('22.05.2021', '4000', '1');
INSERT INTO `Order` (`ORD_DATE`, `ORD_PRICE`, `ORD_USR_ID`) VALUES ('20.06.2021', '35.50', '2');
INSERT INTO `Order` (`ORD_DATE`, `ORD_PRICE`, `ORD_USR_ID`) VALUES ('17.09.2021', '30', '1');
INSERT INTO `Order` (`ORD_DATE`, `ORD_PRICE`, `ORD_USR_ID`) VALUES ('13.12.2021', '5500', '3');
INSERT INTO `Order` (`ORD_DATE`, `ORD_PRICE`, `ORD_USR_ID`) VALUES ('05.01.2022', '3500', '1');
INSERT INTO `Order` (`ORD_DATE`, `ORD_PRICE`, `ORD_USR_ID`) VALUES ('11.02.2022', '1530', '3');

INSERT INTO `Cart` (`CRT_ORD_ID`, `CRT_PRO_ID`) VALUES ('1', '2');
INSERT INTO `Cart` (`CRT_ORD_ID`, `CRT_PRO_ID`) VALUES ('2', '1');
INSERT INTO `Cart` (`CRT_ORD_ID`, `CRT_PRO_ID`) VALUES ('2', '3');
INSERT INTO `Cart` (`CRT_ORD_ID`, `CRT_PRO_ID`) VALUES ('3', '3');
INSERT INTO `Cart` (`CRT_ORD_ID`, `CRT_PRO_ID`) VALUES ('4', '2');
INSERT INTO `Cart` (`CRT_ORD_ID`, `CRT_PRO_ID`) VALUES ('4', '4');
INSERT INTO `Cart` (`CRT_ORD_ID`, `CRT_PRO_ID`) VALUES ('5', '4');
INSERT INTO `Cart` (`CRT_ORD_ID`, `CRT_PRO_ID`) VALUES ('6', '3');
INSERT INTO `Cart` (`CRT_ORD_ID`, `CRT_PRO_ID`) VALUES ('6', '5');
